module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "jest": true,
    },
    "plugins": ["jest"],
    "extends": "airbnb",
    "rules": {
        "jsx-a11y/anchor-is-valid": ["error", {
            // tells eslint that for 'Link' an 'to' attribute is also ok instead of a 'href'
            "components": ["Link"],
            "specialLink": ["to"]
        }],
        "react/jsx-one-expression-per-line": 0,
        "react/destructuring-assignment": 0, // I don't like this rule for readability resaons
        "max-len": ["error", {
          "code": 110,
          "tabWidth": 2,
          "ignoreUrls": true,
          "ignoreComments": true
        }],
        "no-plusplus": 0,
        "require-jsdoc": ["error", {
            "require": {
              "FunctionDeclaration": true,
              "ClassDeclaration": true,
              "MethodDefinition": true,
              "ArrowFunctionExpression": false,
              "FunctionExpression": true
            }
          }],
          "valid-jsdoc": ["error", {
            "prefer": {
              "return": "returns"
            },
            "requireReturnType": true,
            "requireParamType": true,
            "requireParamDescription": true,
            "requireParamType": true
        }],
        "import/first": 0,
        "import/order": 0,
    },
    "settings": {
        "import/resolver": {
            "node": {
                // tells eslint, that webpack is also looking for modules in 'src' folder
                "paths": ["src"]
            },
        },
    },
};
