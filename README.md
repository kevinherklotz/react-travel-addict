# React based website of Travel Addict

This is the code base for the website travel-addict.de. Even though it's not an complex website it is written in React and I try to build it like you would build a large, scalable, tested website. I know React might not be the right choice for a website like this, but I do this to learn React and all the other tools around it.

## Aims

* (ok) create a "fundament" based on npm
* keep README up to date (installation guides, ...)
* (ok) bundling with Webpack
* (ok) minify code
* Ecmascript:
    * (ok) Use JSDoc
    * (ok) Lint JSX with eslint
    * develop test driven with Jest
* Style:
    * (ok) Use SCSS
    * (ok) include postcss with autoprefixer
    * (ok) lint SCSS with stylelint

### Future Ideas

* Update *stylelint* to version 9 as soon as *stylelint-webpack-plugin* gets updated.
* Add Google Analytics for statistics.
* Add cross browser UI tests. ([good article](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Cross_browser_testing))
* Use [tree shaking for font awesome](https://fontawesome.com/how-to-use/use-with-node-js#tree-shaking) to not load all 874 icons.
* [Make it a Progressive Web App](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#making-a-progressive-web-app) to work offline, because travellers often don't have network.
* Add [Manifest](https://developers.google.com/web/fundamentals/web-app-manifest/) to let users add the app to their homescreen.

## TODOs

* add Modernizr, because hover on front page for example isn't available on touch screens (use .no-touchevents, which is already used in stylesheets)
* remove the todos in code
* add snapshot testing for components that are (almost) "final"
 (see min 30 in "Building a progressive web application with React - Testing with Jest - Part 4" on Youtube)

---

## Focus of the project

* easy to understand code base for new developers
* small final file size for fast page load (<1MB for all files)
    * (testable with **`npm run dashboard`**)

## Supported browsers

See .browserlistrc

## Miscellaneous / choice of tools and features
* I thought about using **future CSS** with [postcss-preset-env](https://preset-env.cssdb.org/), but a big (and important) part of the new syntax is still an editors draft, an "should be considered highly unstable and subject to change" ([Source](https://cssdb.org/#staging-process)). Aside from that I don't see any advantages compared to SCSS yet.

* AS I still want to support Internet Explorer 11 I'm not able to use the supercool CSS feature **CSS Grid**.

---

## Development Process

### Requirements

* NodeJS >= 10.1.0

### Local installation for development

Check out project and install NPM dependencies with the command **`npm install`**

### Developing

All the source files of the web app are located in the *src/* folder.
For developing you just need to run **`npm run dev`** to start [*Webpack*](https://webpack.js.org/) and a the website will automatically open in your browser. As you do any changes to the source files the website will automatically refresh.

#### Test

**Ecmascript**

Ecmascript source files are linted by [*ESLint*](http://eslint.org/) (syntax checks). This happens automatically during development with *Webpack*

There should be also a test file for every .js/.jsx file (not all of them might be written yet). Those **unit tests** will NOT run automatically within the Webpack process. You should run them in an extra terminal with the command **`npm test`** - this will start [*Jest*](http://jestjs.io/) in the watch mode.

For coverage reports use **`npm run test:coverage`**.

Unfortunately webpack doesn't run *ESLint* in test files (because they are never included), so you have to check them with the separate command **`npm run eslint`**. The aim is to find a better solution for that.

**Stylesheets**

SCSS Stylesheets are automatically linted by [*Stylelint*](https://stylelint.io/) during development with *Webpack*.

### Production Build

To generate the web app for production run **`npm run build`**. Tests (*Jest*) and lints (*Stylelint* and *ESLint*) must pass otherwise the build will fail.
All the generated files will be minified and saved in the *dist/* folder.

---

## Useful tools, that are not necessarily needed
* After building (**`npm run build`**) you can run **`npm run dashboard`** to see, if some packages might be too big in size.
* To check if there are updates available there is a tool called *npm-check* - install it globally, not in the project:
  ```sh
  npm install -g npm-check
  ```

    * check for updates in current directory: **`npm-check -u`**
