const config = {
  rootDir: 'src',
  moduleNameMapper: {
    // mock files/assets:
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.js',
    // mock stylesheets:
    '\\.(css|scss)$': 'identity-obj-proxy',
  },
  setupTestFrameworkScriptFile: '<rootDir>/../jest.setup.js',
  // tells jest to resolve module imports in this folder:
  modulePaths: ['<rootDir>'],
  coverageDirectory: '../.tmp/coverage/',
};

module.exports = config;
