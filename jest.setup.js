const Enzyme = require('enzyme');
const EnzymeAdapter = require('enzyme-adapter-react-16');

// "Enzyme is a JavaScript Testing utility for React that makes it easier to assert,
//  manipulate, and traverse your React Components' output."

// Setup enzyme's react adapter
Enzyme.configure({ adapter: new EnzymeAdapter() });
