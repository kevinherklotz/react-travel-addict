import React from 'react';
import { Route, Switch } from 'react-router-dom';
import TopBar from 'components/TopBar';
import Footer from 'components/Footer';
import ScrollToTop from 'components/ScrollToTop';
import FrontPage from 'pages/FrontPage';
import Canada from 'pages/Canada';
import NewZealand from 'pages/NewZealand';
import AboutMe from 'pages/AboutMe';
import NotFoundPage from 'pages/NotFoundPage';
import TravelTips from 'pages/travel-tips/TravelTips';
import PackingList from 'pages/travel-tips/PackingList';
import Preparations from 'pages/travel-tips/Preparations';
import UsefulApps from 'pages/travel-tips/UsefulApps';
import SiteNotice from 'pages/SiteNotice';
import './Scaffolding.scss';

const Scaffolding = () => (
  <ScrollToTop>
    <div className="no-touchevents">
      <Route component={TopBar} />

      <div className="content shadow shadow-inset shadow-top">
        <main>
          <Switch>
            <Route exact path="/" component={FrontPage} />
            <Route exact path="/kanada" component={Canada} />
            <Route exact path="/neuseeland" component={NewZealand} />
            <Route exact path="/reisetipps" component={TravelTips} />
            <Route path="/reisetipps/packliste" component={PackingList} />
            <Route path="/reisetipps/vorbereitungen" component={Preparations} />
            <Route path="/reisetipps/apps" component={UsefulApps} />
            <Route path="/ueber-mich" component={AboutMe} />
            <Route path="/impressum" component={SiteNotice} />
            <Route component={NotFoundPage} />
          </Switch>
        </main>
        <Footer />
      </div>
    </div>
  </ScrollToTop>
);

export default Scaffolding;
