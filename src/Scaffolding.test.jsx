import React from 'react';
import { mount } from 'enzyme';
import Scaffolding from './Scaffolding';
import { MemoryRouter } from 'react-router-dom';

import FrontPage from 'pages/FrontPage';
import TopBar from 'components/TopBar';
import Footer from 'components/Footer';
import NotFoundPage from 'pages/NotFoundPage';
import AboutMe from 'pages/AboutMe';
import TravelTips from 'pages/travel-tips/TravelTips';
import PackingList from 'pages/travel-tips/PackingList';
import Preparations from 'pages/travel-tips/Preparations';
import UsefulApps from 'pages/travel-tips/UsefulApps';
import SiteNotice from 'pages/SiteNotice';

const routerComponentRoot = mount( // eslint-disable-line function-paren-newline
  <MemoryRouter initialEntries={['/']} initialIndex={0}>
    <Scaffolding />
  </MemoryRouter>,
); // eslint-disable-line function-paren-newline

describe('Rendering of root routes', () => {
  it('should render FrontPage', () => {
    expect(routerComponentRoot.find(FrontPage).length).toBe(1);
  });
  it('should render TopBar', () => {
    expect(routerComponentRoot.find(TopBar).length).toBe(1);
  });
  it('should render Footer', () => {
    expect(routerComponentRoot.find(Footer).length).toBe(1);
  });
  it('should NOT render NotFoundPage', () => {
    expect(routerComponentRoot.find(NotFoundPage).length).toBe(0);
  });
});

const routerComponentTravelTips = mount( // eslint-disable-line function-paren-newline
  <MemoryRouter initialEntries={['/reisetipps']} initialIndex={0}>
    <Scaffolding />
  </MemoryRouter>,
); // eslint-disable-line function-paren-newline

describe('Rendering of TravelTips routes', () => {
  it('should render TravelTips', () => {
    expect(routerComponentTravelTips.find(TravelTips).length).toBe(1);
  });
  it('should render TopBar', () => {
    expect(routerComponentTravelTips.find(TopBar).length).toBe(1);
  });
  it('should render Footer', () => {
    expect(routerComponentTravelTips.find(Footer).length).toBe(1);
  });
  it('should NOT render FrontPage', () => {
    expect(routerComponentTravelTips.find(FrontPage).length).toBe(0);
  });
  it('should NOT render NotFoundPage', () => {
    expect(routerComponentTravelTips.find(NotFoundPage).length).toBe(0);
  });
});

const routerComponentPackingList = mount(// eslint-disable-line function-paren-newline
  <MemoryRouter initialEntries={['/reisetipps/packliste']} initialIndex={0}>
    <Scaffolding />
  </MemoryRouter>,
); // eslint-disable-line function-paren-newline

describe('Rendering of PackingList routes', () => {
  it('should render PackingList', () => {
    expect(routerComponentPackingList.find(PackingList).length).toBe(1);
  });
  it('should render TopBar', () => {
    expect(routerComponentPackingList.find(TopBar).length).toBe(1);
  });
  it('should render Footer', () => {
    expect(routerComponentPackingList.find(Footer).length).toBe(1);
  });
  it('should NOT render FrontPage', () => {
    expect(routerComponentPackingList.find(FrontPage).length).toBe(0);
  });
  it('should NOT render TravelTips', () => {
    expect(routerComponentPackingList.find(TravelTips).length).toBe(0);
  });
  it('should NOT render NotFoundPage', () => {
    expect(routerComponentPackingList.find(NotFoundPage).length).toBe(0);
  });
});

const routerComponentPreparations = mount(// eslint-disable-line function-paren-newline
  <MemoryRouter initialEntries={['/reisetipps/vorbereitungen']} initialIndex={0}>
    <Scaffolding />
  </MemoryRouter>,
); // eslint-disable-line function-paren-newline

describe('Rendering of Preparations routes', () => {
  it('should render Preparations', () => {
    expect(routerComponentPreparations.find(Preparations).length).toBe(1);
  });
  it('should render TopBar', () => {
    expect(routerComponentPreparations.find(TopBar).length).toBe(1);
  });
  it('should render Footer', () => {
    expect(routerComponentPreparations.find(Footer).length).toBe(1);
  });
  it('should NOT render FrontPage', () => {
    expect(routerComponentPreparations.find(FrontPage).length).toBe(0);
  });
  it('should NOT render TravelTips', () => {
    expect(routerComponentPreparations.find(TravelTips).length).toBe(0);
  });
  it('should NOT render NotFoundPage', () => {
    expect(routerComponentPreparations.find(NotFoundPage).length).toBe(0);
  });
});

const routerComponentUsefulApps = mount(// eslint-disable-line function-paren-newline
  <MemoryRouter initialEntries={['/reisetipps/apps']} initialIndex={0}>
    <Scaffolding />
  </MemoryRouter>,
); // eslint-disable-line function-paren-newline

describe('Rendering of UsefulApps routes', () => {
  it('should render UsefulApps', () => {
    expect(routerComponentUsefulApps.find(UsefulApps).length).toBe(1);
  });
  it('should render TopBar', () => {
    expect(routerComponentUsefulApps.find(TopBar).length).toBe(1);
  });
  it('should render Footer', () => {
    expect(routerComponentUsefulApps.find(Footer).length).toBe(1);
  });
  it('should NOT render FrontPage', () => {
    expect(routerComponentUsefulApps.find(FrontPage).length).toBe(0);
  });
  it('should NOT render TravelTips', () => {
    expect(routerComponentUsefulApps.find(TravelTips).length).toBe(0);
  });
  it('should NOT render NotFoundPage', () => {
    expect(routerComponentUsefulApps.find(NotFoundPage).length).toBe(0);
  });
});

const routerComponentAboutMe = mount(// eslint-disable-line function-paren-newline
  <MemoryRouter initialEntries={['/ueber-mich']} initialIndex={0}>
    <Scaffolding />
  </MemoryRouter>,
); // eslint-disable-line function-paren-newline

describe('Rendering of AboutMe routes', () => {
  it('should render AboutMe', () => {
    expect(routerComponentAboutMe.find(AboutMe).length).toBe(1);
  });
  it('should render TopBar', () => {
    expect(routerComponentAboutMe.find(TopBar).length).toBe(1);
  });
  it('should render Footer', () => {
    expect(routerComponentAboutMe.find(Footer).length).toBe(1);
  });
  it('should NOT render FrontPage', () => {
    expect(routerComponentAboutMe.find(FrontPage).length).toBe(0);
  });
  it('should NOT render NotFoundPage', () => {
    expect(routerComponentAboutMe.find(NotFoundPage).length).toBe(0);
  });
});

const routerComponentSiteNotice = mount(// eslint-disable-line function-paren-newline
  <MemoryRouter initialEntries={['/impressum']} initialIndex={0}>
    <Scaffolding />
  </MemoryRouter>,
); // eslint-disable-line function-paren-newline

describe('Rendering of SiteNotice routes', () => {
  it('should render SiteNotice', () => {
    expect(routerComponentSiteNotice.find(SiteNotice).length).toBe(1);
  });
  it('should render TopBar', () => {
    expect(routerComponentSiteNotice.find(TopBar).length).toBe(1);
  });
  it('should render Footer', () => {
    expect(routerComponentSiteNotice.find(Footer).length).toBe(1);
  });
  it('should NOT render FrontPage', () => {
    expect(routerComponentSiteNotice.find(FrontPage).length).toBe(0);
  });
  it('should NOT render NotFoundPage', () => {
    expect(routerComponentSiteNotice.find(NotFoundPage).length).toBe(0);
  });
});

const routerComponentNotFoundPage = mount(// eslint-disable-line function-paren-newline
  <MemoryRouter initialEntries={['/x7dtg4j5k788g65d']} initialIndex={0}>
    <Scaffolding />
  </MemoryRouter>,
); // eslint-disable-line function-paren-newline

describe('Rendering of NotFoundPage routes', () => {
  it('should render NotFoundPage', () => {
    expect(routerComponentNotFoundPage.find(NotFoundPage).length).toBe(1);
  });
  it('should render TopBar', () => {
    expect(routerComponentNotFoundPage.find(TopBar).length).toBe(1);
  });
  it('should render Footer', () => {
    expect(routerComponentNotFoundPage.find(Footer).length).toBe(1);
  });
  it('should NOT render FrontPage', () => {
    expect(routerComponentNotFoundPage.find(FrontPage).length).toBe(0);
  });
});

describe('Rendering of main element', () => {
  it('should find exact 1 "main" node', () => {
    expect(routerComponentRoot.find('main').length).toBe(1);
  });
  it('should find exact 1 "main" node', () => {
    expect(routerComponentTravelTips.find('main').length).toBe(1);
  });
  it('should find exact 1 "main" node', () => {
    expect(routerComponentPackingList.find('main').length).toBe(1);
  });
  it('should find exact 1 "main" node', () => {
    expect(routerComponentPreparations.find('main').length).toBe(1);
  });
  it('should find exact 1 "main" node', () => {
    expect(routerComponentUsefulApps.find('main').length).toBe(1);
  });
  it('should find exact 1 "main" node', () => {
    expect(routerComponentAboutMe.find('main').length).toBe(1);
  });
  it('should find exact 1 "main" node', () => {
    expect(routerComponentSiteNotice.find('main').length).toBe(1);
  });
});
