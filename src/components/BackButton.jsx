import React from 'react';
import { Link } from 'react-router-dom';
import './Backbutton.scss';

/**
 * BackButton - button for navigation to go back one hierarchy step
 *
 * @returns {JSX} - JSX that will be injected
 */
export default function BackButton() {
  const path = '/';

  return (
    <div className="py-2 text-inverse">
      <Link to={path} className="back-button no-anchor-style">
        <span className="fas fa-arrow-left" />
        <span className="description">
          <span>
            zurück
          </span>
        </span>
      </Link>
    </div>
  );
}
