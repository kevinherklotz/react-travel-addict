import React from 'react';
import { shallow } from 'enzyme';
import BackButton from './BackButton';


describe('BackButton component rendering', () => {
  const component = shallow(<BackButton />);

  it('should contain the word zurück', () => {
    expect(component.contains('zurück')).toBe(true);
  });

  it('Link should have class .back-button', () => {
    expect(component.find('Link').hasClass('back-button')).toBe(true);
  });
});

// todo
// describe('BackButton correct links', () => {
//   it('link with given path');
// });
