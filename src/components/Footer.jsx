import React from 'react';
import { Link } from 'react-router-dom';

/**
 * Footer - at the very bottom on every page
 *
 * @returns {JSX} - JSX that will be injected
 */
export default function Footer() {
  return (
    <footer className="text-center py-1">
      <a
        href="https://www.facebook.com/real.travel.addict"
        target="_blank"
        rel="noopener noreferrer"
        title="Travel-Addict auf facebook"
      >
        <span className="fab fa-facebook text-fb" />
      </a>
      {' | '}
      <a
        href="https://www.youtube.com/c/TravelAddict"
        target="_blank"
        rel="noopener noreferrer"
        title="Travel-Addict auf YouTube"
      >
        <span className="fab fa-youtube text-yt" />{' '}
      </a>
      {' | '}
      <a
        href="http://instagram.com/real_travel_addict/"
        target="_blank"
        rel="noopener noreferrer"
        title="Travel-Addict auf Instagram"
      >
        <span className="fab fa-instagram text-insta" />{' '}
      </a>
      {true && (
        <span>
          | <Link to="/impressum">Impressum</Link>
        </span>
      )}
    </footer>
  );
}
