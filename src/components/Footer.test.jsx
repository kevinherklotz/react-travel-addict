import React from 'react';
import { shallow } from 'enzyme';
import Footer from './Footer';


describe('Footer component should render as expected', () => {
  const component = shallow(<Footer />);

  it('should contain the word "Impressum"', () => {
    expect(component.contains('Impressum')).toBe(true);
  });

  it('should have 3 static links and 1 React "Link"', () => {
    expect(component.find('Link').length).toBe(1);
    expect(component.find('a').length).toBe(3);
  });
});
