import { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

/* eslint-disable max-len */

/**
 * Component that scrolls to top when Router component updates
 *
 * Created this component according to the following official manual:
 * https://github.com/ReactTraining/react-router/blob/v4.3.1/packages/react-router-dom/docs/guides/scroll-restoration.md
 */
class ScrollToTop extends Component {
  /**
   * Scroll to top on
   * @param {Object} prevProps - Properties before update
   * @returns {undefined}
   */
  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      window.scrollTo(0, 0);
    }
  }
  /* eslint-enable max-len */

  /**
   * Render children of ScrollToTop
   *
   * @returns {JSX} - JSX that is surrounded by <ScrollToTop>
   */
  render() {
    return this.props.children;
  }
}

ScrollToTop.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default withRouter(ScrollToTop);
