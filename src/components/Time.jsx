import React from 'react';
import moment from 'moment-timezone';
import PropTypes from 'prop-types';

/**
 * Day and time of the current travel destination
 */
export default class Time extends React.Component {
  /**
   * Constructor
   * @param {object} props - Parent props
   */
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
    };
  }

  /**
   * React componentDidMount
   * set timer every 1 second
   *
   * @returns {undefined}
   */
  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  /**
   * React componentWillUnmount
   * destroy timer
   *
   * @returns {undefined}
   */
  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  /**
   * Update Date in state
   *
   * @returns {undefined}
   */
  tick() {
    this.setState({
      date: new Date(),
    });
  }

  /**
   * Render Timer span
   *
   * @returns {HTML} HTML that will be injected
   */
  render() {
    const momentTime = moment(this.state.date).tz(this.props.timezone).locale('de');
    return (
      <time>
        <span className="hidden-xs-down">{momentTime.format('dddd')} </span>
        {momentTime.format('k:mm')}
      </time>
    );
  }
}

Time.propTypes = {
  timezone: PropTypes.string.isRequired,
};
