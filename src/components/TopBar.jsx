import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Time from './Time';
import TravelLocations from './TravelLocation';
import timeFunctions from './timeFunctions';

/**
 * TopBar - the very top bar on every page
 *
 * @param {Object} props - The properties that react-router gives as parameter
 * @param {Object} props.match - Contains information about how a <Route path> matched the URL
 * @param {string} props.match.url - The path pattern used to match
 * @returns {JSX} - JSX that will be injected
 */
export default class TopBar extends React.Component {
  /**
   * Check if user is on the root ("front") page
   *
   * @returns {boolean} - true if it's root
   */
  isRoot() {
    return this.props.location.pathname === '/';
  }

  /**
   * Render TopBar
   *
   * @returns {JSX} - asd
   */
  render() {
    const travelLocation = new TravelLocations(new Date());
    const totalDaysTitle = 'Ich bin jetzt schon seit dem 10.9.2016 auf Reisen';
    const mapsLinkTitle = 'hier bin ich gerade (in Google Maps öffnen)';
    const timeTitle = `aktuelle Uhrzeit in ${travelLocation.getcurrentCountry()}`;

    return (
      <div className="bg-inverse text-inverse">
        <div className="container py-1">
          <div className="media small">
            {!this.isRoot() && (
              <nav className="media-figure">
                <Link to="/">Travel-Addict</Link>
              </nav>
            )}

            <div className="media-figure text-right pl-2">
              <span className="mr-1 hidden-xs-down" title={totalDaysTitle}>
                {/* shows amount of days I'm travelling already */}
                <span className="fas fa-calendar-alt" />&nbsp; Tag&nbsp;{timeFunctions.getTotalDaysTillNow()}
              </span>
              <span>|</span>
              <span>
                <a
                  className="ml-1 mr-1"
                  href={travelLocation.getGoogleMapsLink()}
                  target="_blank"
                  rel="noopener noreferrer"
                  title={mapsLinkTitle}
                >
                  <span className="fas fa-globe" />&nbsp;
                  {travelLocation.getTravelLocation()}
                </a>
              </span>
              |
              <span className="ml-1" title={timeTitle}>
                <span className="fas fa-clock-o" />&nbsp;
                <Time timezone={travelLocation.getTravelTimezone()} />
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

TopBar.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
};
