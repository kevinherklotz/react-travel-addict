import travelLocations from '../data/travelLocations';
import timeFunctions from './timeFunctions';

/**
 * Locations Class
 *
 * Get infos about location based on a date, because my location changes often while travelling
 */
export default class TravelLocations {
  /**
   * Constructor
   *
   * @param {Date} date Date that is used to look for current location
   * @param {Array} locationsArray Array of location objects that contain 'from', 'location', 'googleMapsID', and 'timezone'
   */
  constructor(date, locationsArray = travelLocations) {
    if (!Array.isArray(locationsArray)) {
      throw new Error('locationsArray must be an array');
    }

    this.date = date;
    this.locationsArray = locationsArray;

    this.locationObject = this.getPresentLocationObject();

    this.currentCountry = 'Australien'; // todo: add countries to list tavelLocations and get value from there
  }

  /**
   * Searches for the last location (based on this.date) and if it doesn't find anything it returns a generic object
   *
   * @returns {{from: [number,number,number,number], location: string, googleMapsID: string, timezone: string}} Present location object
   */
  getPresentLocationObject() {
    // I can't modify the locationsArray, because this would change the original dateArray
    const locationsArrayCopy = Array.from(this.locationsArray);

    // return last element of locationsArrayCopy that matches the find callback
    let locationObject = locationsArrayCopy
      .reverse()
      .find(el => timeFunctions.getDateFromArray(el.from) < this.date);

    // generic object if no object was found
    if (!locationObject) {
      locationObject = {
        from: [1970, 1, 1, 0],
        location: 'Unbekannt',
        googleMapsID: '',
        timezone: 'Pacific/Auckland',
      };
    }

    return locationObject;
  }

  /**
   * Get name of current location
   *
   * @returns {string} Name of current location
   */
  getTravelLocation() {
    return this.locationObject.location;
  }

  /**
   * Get current Timezone
   *
   * @returns {string} Timezone of current location
   */
  getTravelTimezone() {
    return this.locationObject.timezone;
  }

  /**
   * Get name of current Country
   * right now its just a static value
   * todo: needs to return a calculated country
   *
   * @returns {string} Name of current country
   */
  getcurrentCountry() {
    return this.currentCountry;
  }

  /**
   * Get Google Maps Link
   *
   * @returns {string} Link to Google Maps
   */
  getGoogleMapsLink() {
    if (this.locationObject.googleMapsID === '') {
      return `https://www.google.de/maps?q=${this.locationObject.location}`;
    }

    return `https://goo.gl/maps/${this.locationObject.googleMapsID}`;
  }
}
