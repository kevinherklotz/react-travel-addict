import TravelLocation from './TravelLocation';

const travelLocationMockArray = [
  {
    from: [2016, 12, 21, 12],
    location: 'Town A',
    googleMapsID: '',
    timezone: 'Pacific/Auckland',
  },
  {
    from: [2016, 12, 29, 13],
    location: 'Town B',
    googleMapsID: '',
    timezone: 'America/Toronto',
  },
  {
    from: [2050, 1, 2, 10],
    location: 'Town C',
    googleMapsID: '',
    timezone: 'Asia/Taipei',
  },
];

describe('TravelLocation constructor error handling', () => {
  it('TravelLocation should throw error when string is passed instead of array', () => {
    const wrongFunction = () => { new TravelLocation(new Date('2016/12/23'), 'string'); }; // eslint-disable-line no-new
    expect(wrongFunction).toThrowError(/must be an array/);
  });
});

describe('Function getPresentLocationObject()', () => {
  it('should return the first object in array', () => {
    const travelLocation = new TravelLocation(new Date('2016/12/23'), travelLocationMockArray);
    expect(travelLocation.getPresentLocationObject(travelLocationMockArray)).toBe(travelLocationMockArray[0]);
  });
  it('should return the second object in array', () => {
    const travelLocation = new TravelLocation(new Date('2016/12/30'), travelLocationMockArray);
    expect(travelLocation.getPresentLocationObject()).toBe(travelLocationMockArray[1]);
  });
});

describe('Function getTravelLocation()', () => {
  it('should return "Town A"', () => {
    const travelLocation = new TravelLocation(new Date('2016/12/23'), travelLocationMockArray);
    expect(travelLocation.getTravelLocation()).toBe('Town A');
  });
  it('should return "Unbekannt"', () => {
    const travelLocation = new TravelLocation(new Date('2000/12/23'), travelLocationMockArray);
    expect(travelLocation.getTravelLocation()).toBe('Unbekannt');
  });
});

describe('Function getTravelTimezone()', () => {
  it('should return "Pacific/Auckland"', () => {
    const travelLocation = new TravelLocation(new Date('2016/12/23'), travelLocationMockArray);
    expect(travelLocation.getTravelTimezone()).toBe('Pacific/Auckland');
  });
  it('should return "Pacific/Auckland"', () => {
    const travelLocation = new TravelLocation(new Date('1920/12/23'), travelLocationMockArray);
    expect(travelLocation.getTravelTimezone()).toBe('Pacific/Auckland');
  });
});

describe('Function getcurrentCountry()', () => {
  it('should return "Pacific/Auckland"', () => {
    const travelLocation = new TravelLocation(new Date('2016/12/23'), travelLocationMockArray);
    expect(travelLocation.getcurrentCountry()).toBe('Australien');
  });
});
