import moment from 'moment-timezone';

const timeFunctions = {
  arrivalTime: [2016, 9, 10, 13], // year, month, day, hour
  arrivalTimezone: 'Pacific/Auckland',

  /**
   * Returns difference between time of arrival and now
   *
   * @returns {string} amount of days since I am in my travel country
   */
  getTotalDaysTillNow: () => {
    const oneDay = 24 * 60 * 60 * 1000;
    const currentDate = timeFunctions.getCurrentDateFromTimezone(timeFunctions.arrivalTimezone);
    const arrivalDate = timeFunctions.getDateFromArray(timeFunctions.arrivalTime);
    const diff = currentDate - arrivalDate;

    return Math.floor(diff / oneDay);
  },

  /**
   * Returns a Date object of the current date in a given timezone
   *
   * @param  {String} tz Timezone (like 'Europe/Berlin')
   * @returns {Date}      Current Date in given Timezone
   */
  getCurrentDateFromTimezone: (tz) => {
    // get the "now" from certain timezone:
    const now = moment().tz(tz);
    return now.toDate();
  },


  /**
   * Returns date from array with correct month
   * (decrements the second array value, because months are zero based)
   *
   * @param  {array} dateArray date array like [2016, 1, 16] for 16th of January
   * @returns {Date} Date calculated from dateArray
   */
  getDateFromArray: (dateArray) => {
    // I can't modify the dateArray, because this would change the original dateArray
    const arrayCopy = Array.from(dateArray);

    // decrement month, because it starts with month 0
    arrayCopy[1]--;

    return new Date(...arrayCopy);
  },
};

export default timeFunctions;
