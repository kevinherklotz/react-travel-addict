// todo: update list
const travelLocations = [
  {
    from: [2016, 12, 21, 12],
    location: 'Wanaka',
    googleMapsID: '',
    timezone: 'Pacific/Auckland',
  },
  {
    from: [2016, 12, 29, 13],
    location: 'Christchurch',
    googleMapsID: '',
    timezone: 'Pacific/Auckland',
  },
  {
    from: [2017, 1, 2, 10],
    location: 'Kaikoura',
    googleMapsID: '',
    timezone: 'Pacific/Auckland',
  },
  {
    from: [2017, 1, 25, 10],
    location: 'Takaka',
    googleMapsID: '',
    timezone: 'Pacific/Auckland',
  },
  {
    from: [2017, 2, 11, 10],
    location: 'Nelson',
    googleMapsID: '',
    timezone: 'Pacific/Auckland',
  },
  {
    from: [2017, 2, 21, 10],
    location: 'Blenheim',
    googleMapsID: '',
    timezone: 'Pacific/Auckland',
  },
  {
    from: [2017, 2, 24, 10],
    location: 'Picton',
    googleMapsID: '',
    timezone: 'Pacific/Auckland',
  },
  {
    from: [2017, 2, 25, 10],
    location: 'Marlborough Sounds',
    googleMapsID: '',
    timezone: 'Pacific/Auckland',
  },
  {
    from: [2017, 2, 26, 17],
    location: 'Wellington',
    googleMapsID: '',
    timezone: 'Pacific/Auckland',
  },
  {
    from: [2017, 3, 7, 12],
    location: 'Mt Taranaki',
    googleMapsID: '',
    timezone: 'Pacific/Auckland',
  },
  {
    from: [2017, 4, 28, 12],
    location: 'Auckland',
    googleMapsID: '',
    timezone: 'Pacific/Auckland',
  },
  {
    from: [2017, 6, 21, 2],
    location: 'Fidschi',
    googleMapsID: '',
    timezone: 'Pacific/Auckland',
  },
  {
    from: [2017, 7, 9, 18],
    location: 'Cairns',
    googleMapsID: '',
    timezone: 'Australia/Brisbane',
  },
];

export default travelLocations;
