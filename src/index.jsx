import React from 'react';
import ReactDOM from 'react-dom';
import Scaffolding from 'Scaffolding';
import { BrowserRouter as Router } from 'react-router-dom';
import './style/framework.scss';

ReactDOM.render(
  <Router>
    <Scaffolding />
  </Router>,
  document.getElementById('app'),
);
