import React from 'react';
import BackButton from 'components/BackButton';

/**
 * AboutMe - content of page "Über mich"
 *
 * @returns {JSX} - JSX that will be injected
 */
export default function AboutMe() {
  return (
    <div className="container pt-3">
      <article>
        <h1>Das bin ich</h1>
        <p>
          Hallo ich heiße Kevin und bin 27 Jahre alt.
          <br />
          Vor einigen Jahren waren mir materielle Dinge viel wichtiger als immaterielle Dinge wie Reisen. Das
          hat sich geändert, nachdem ich 2014 für 7 Monate in Kanada war. Ich habe erkannt, dass Reichtum
          nichts mit Geld oder materiellen Dingen zu tun hat, sondern viel mehr mit Lebensfreude und
          Lebenserfahrung.
        </p>
        <p>
          Seit Kanada habe ich auch das Fotografieren für mich entdeckt und seitdem ich in Neuseeland bin habe
          ich auch Spaß daran gefunden Videos zu machen. Und da ich die letzten 2 Jahre als Web-Entwickler
          tätig war, habe ich einfach dazu entschieden meine eigene Webseite zu programmieren, auf der ich
          meine Fotos, Videos und auch andere Sachen zeigen kann. Außerdem war mein Ziel mit dieser Webseite
          mehr Erfahrungen mit Programmieren zu bekommen, was einen späteren Berufseinstieg sicher deutlich
          einfacher macht.
        </p>
        <p>
          Durch die Fotografie, die Videos und auch diese Webseite hier habe ich die Möglichkeit kreativ zu
          sein und immer wieder Neues zu lernen. Und ich freue mich einfach daran das mit euch zu teilen.
          Außerdem wird es in ein paar Jahren eine wunderschöne Erinnerung für mich sein.
        </p>
        <p>
          Das meiste Wissen über Fotografie, Web-Programmierung und Videoschnitt habe ich mir übrigens selbst
          im Internet (meistens durch Youtube) angeeignet und auch vor meiner Reise habe ich mir auch viele
          andere Youtube-Channels übers Reisen angeschaut, die mich inspiriert haben - vielleicht kann ich ja
          den ein oder anderen auch mit meinen Youtube-Videos inspirieren.
        </p>
        <p>
          Übrigens verdiene ich mit der Webseite und meinen Youtube Videos kein Geld und das war auch nie mein
          Ziel. Ich habe zwar hier und da ein paar Amazon-Affiliate Links und ich hatte auch Werbung vor meine
          Videos geschalten (als das noch für kleine Youtuber wie mich erlaubt war), das bisschen refinanziert
          aber nicht einmal meine monatlichen Kosten für die Webseite - von den Kosten für die dafür
          erforderliche Software und das Equipment mal ganz abgesehen. Dennoch sehe ich das ganze als ein
          Gewinn für mich - denn ich habe Spaß dabei und lerne etwas dabei.
        </p>
        <p>
          Ich werde in meinen Videos auch nicht um Likes und Abos betteln, da mich das bei anderen Youtubern
          immer nervt. Leider wird man bei Youtube auch öfter vorgeschlagen, wenn man viele Videos hochlädt -
          ich setze aber lieber auf Qualität, statt Quantität, auch wenn ich dadurch weniger Klicks habe.
          Daily Vlogs kommen für mich deshalb nie in Frage.
          <br />
          Videos schneiden hat bei mir auch nicht immer höchste Priorität, Spaß haben ist wichtiger und
          deshalb dauert es manchmal etwas länger bis zum nächsten Video - immerhin dauert es ca. 10-20
          Arbeitsstunden, um ein Video zu schneiden. Dabei versuche ich auch immer ein paar Infos zum
          jeweiligen Ort einzubauen, auch wenn das manchen zu viel &quot;Gelaber&quot; ist. Das ist einfach
          mein Stil und ich hoffe er gefällt manchen.
        </p>
        <p className="bg-inverse text-inverse">
          TODO: <br />
          Bild einfügen; zu viel Text...etwas kürzen, vielleicht irgendwie nach folgendem Schema:
        </p>
        <p>Meine Hauptmotivationen fürs Vloggen sind:</p>
        <ol>
          <li>eine persönliche Erinnerung für später zu haben</li>
          <li>
            meinen Freunden und meiner Familie zu zeigen, was ich erlebe und wie schön es auf der anderen
            Seite der Welt ist
          </li>
          <li>anderen Reisenden Tipps zu geben und einen Eindruck vom Reiseland zu verschaffen</li>
          <li>Missstände aufzudecken und zu bilden</li>
          <li>Freude am Filmen und Fotografieren</li>
        </ol>
      </article>

      <BackButton />
    </div>
  );
}
