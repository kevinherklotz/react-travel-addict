import React from 'react';
import BackButton from 'components/BackButton';

/**
 * Canada - Render Canada page
 *
 * @returns {JSX} - JSX that will be injected
 */
export default function Canada() {
  return (
    <div className="container pt-3">
      <article>
        <h1>Kanada</h1>
        <p>
          Im Jahr 2014 war ich für 7 Monate in Kanada, um dort Work and Travel zu machen. Die meiste Zeit habe
          ich mich in den Rocky Mountains aufgehalten und dort für verschiedene Familien gearbeitet. Gegen
          Ende meiner Reise bin ich noch viel herum gereist. Ich bin von Vancouver Island bis nach Québec City
          mit dem Auto gefahren und habe dadurch viel vom Süden Kanadas gesehen. Am Ende war ich sogar noch
          Eine Woche in New York City.
        </p>
        <p>
          Ich habe mal einen Blog über meinen Kanada-Aufenthalt geschrieben, allerdings gibt es den nicht
          mehr. Ich versuche also die alten Inhalte irgendwie auf dieser neue Webseite einzubinden. Das dauert
          aber noch eine Weile und hat bei mir vorerst niedrige Priorität.
        </p>
        <p>
          Du kannst dir aber gerne die besten Fotos meiner Kanadareise{' '}
          <a href="https://www.flickr.com/photos/114243505@N07/albums/72157649465216300">hier auf Flickr</a>{' '}
          anschauen.
        </p>
      </article>

      <BackButton />
    </div>
  );
}
