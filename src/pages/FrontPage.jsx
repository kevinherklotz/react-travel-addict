import React from 'react';
import { Link } from 'react-router-dom';
import './FrontPage.scss';
import imgMyself from '../assets/images/myself.jpg';
import imgNZ from '../assets/images/main-image-nz.jpg';
import imgCA from '../assets/images/main-image-ca.jpg';
import travelTips from '../assets/images/travel-tips.jpg';
import aboutMe from '../assets/images/about-me.jpg';

/**
 * Front Page - content of the root page
 *
 * @returns {JSX} - JSX that will be injected
 */
export default function FrontPage() {
  return (
    <div>
      <Header />
      <div>
        <img className="avatar center-block circled" src={imgMyself} alt="das bin ich" />
      </div>
      <div className="container">
        <article>
          <h2>Willkommen auf meinem Reiseblog</h2>
          <p>
            Schön, dass du auf meiner Seite gelandet bist.
            Momentan ist sie noch nicht ganz fertig, proportional zu den Regentagen hier in Neuseeland
            wird sie aber stetig verbessert.
          </p>
        </article>
        <div className="row">
          <div className="col-xl-6">
            <Link to="/neuseeland" className="no-anchor-style">
              <div className="main-destination-wrapper shadow hover-scale">
                <img src={imgNZ} alt="Bild von Neuseeland" />
                <div className="description">
                  <h2>Neuseeland</h2>
                  <span>2016 - 2017</span>
                </div>
              </div>
            </Link>
          </div>
          <div className="col-xl-6">
            <Link to="/kanada" className="no-anchor-style">
              <div className="main-destination-wrapper shadow hover-scale">
                <img src={imgCA} alt="Bild von Kanada" />
                <div className="description">
                  <h2>Kanada</h2>
                  <span>2014</span>
                </div>
              </div>
            </Link>
          </div>
        </div>


        <div className="row mt-4">
          <div className="col-xs-1 hidden-md-down hidden-xl-up" />
          <div className="col-xs-12 col-md-6 col-lg-4 col-xl-6">
            <Link to="/reisetipps" className="no-anchor-style">
              <div className="shadow hover-scale">
                <div className="row">
                  <div className="col-xs-12 col-sm-6 col-md-12 col-xl-6">
                    <img src={travelTips} alt="Reisetipps" className="img-responsive w-100" />
                  </div>
                  <div className="col-xs-12 col-sm-6 col-md-12 col-xl-6">
                    <h3 className="pt-2 pl-1">Reisetipps</h3>
                    <ul className="fa-ul">
                      <li><span className="fa-li fa fa-suitcase" />Packliste</li>
                      <li><span className="fa-li fa fa-list" />Vorbereitungen</li>
                      <li><span className="fa-li fa fa-mobile" />nützliche Apps</li>
                    </ul>
                  </div>
                </div>
              </div>
            </Link>
          </div>
          <div className="col-xs-2 hidden-md-down hidden-xl-up" />
          <div className="p-3 hidden-md-up" />
          <div className="col-xs-12 col-md-6 col-lg-4 col-xl-6">
            <Link to="/ueber-mich" className="no-anchor-style">
              <div className="shadow h-100 hover-scale">
                <div className="row">
                  <div className="col-xs-12 col-sm-6 col-md-12 col-xl-6">
                    <img src={aboutMe} alt="Über mich" className="img-responsive w-100" />
                  </div>
                  <div className="col-xs-12 col-sm-6 col-md-12 col-xl-6">
                    <h3 className="py-2 pl-1">Über mich</h3>
                  </div>
                </div>
              </div>
            </Link>
          </div>
          <div className="col-xs-1 hidden-md-down hidden-xl-up" />
        </div>
      </div>
    </div>
  );
}

/**
 * Header with Background image and cite
 *
 * @returns {JSX} - JSX that will be injected
 */
function Header() {
  return (
    <div className="front-page-header text-center">
      <h1>Travel-Addict</h1>
      <blockquote>Collect moments, not things</blockquote>
    </div>
  );
}
