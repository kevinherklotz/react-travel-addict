import React from 'react';
import BackButton from 'components/BackButton';

/**
 * NewZealand - Render New Zealand page
 *
 * @returns {JSX} - JSX that will be injected
 */
export default function NewZealand() {
  return (
    <div className="container pt-3">
      <article>
        <h1>Neuseeland</h1>
        <p>
          Derzeit befinde ich mich in Neuseeland und versuche hier auf dieser Seite noch mehr Infos darüber
          bereitzustellen: Fotos, Karten, einen Zeitstrahl, ein paar kurze Texte. So ist zumindest der Plan -
          derzeit musst du dich aber &quot;nur&quot; mit den Videos zufrieden geben ;-)
        </p>
        <h4>Viel Spaß!</h4>
      </article>

      <div className="responsive-video mb-3">
        <iframe
          title="YouTube Playlist Neuseeland"
          width="1200"
          height="675"
          src="https://www.youtube.com/embed/HpzXavCYhQw"
          frameBorder="0"
          allow="autoplay; encrypted-media"
          allowFullscreen
        />
      </div>

      <BackButton />
    </div>
  );
}
