import React from 'react';
import { Link } from 'react-router-dom';

/**
 * AboutMe - Render Page, that is shown when a wrong URL is opened
 *
 * @returns {JSX} - JSX that will be injected
 */
export default function NotFoundPage() {
  return (
    <div className="container">
      <h1 className="py-4">404 - Seite nicht gefunden!</h1>
      <p>Diese Seite gibt es leider nicht.</p>
      <p>
        <Link to="/">
          <span className="fas fa-undo" /> zurück zur Startseite
        </Link>
      </p>
    </div>
  );
}
