import React from 'react';
import BackButton from 'components/BackButton';

/**
 * SiteNotice - Render Impressum Page // todo: renew imprint according to DSGVO
 *
 * @returns {JSX} - JSX that will be injected
 */
export default function SiteNotice() {
  return (
    <article className="container">
      <h1>Impressum</h1>

      <p>Angaben gemäß § 5 TMG</p>

      <p>
        Kevin H<span className="bot-obfuscation" />
        <br />
        Dorfstraße 128a<br />
        09623 Clausnitz<br />
      </p>
      <h2>Kontakt:</h2>
      <p>
        E-Mail: h<span className="bot-obfuscation">.kevin@gm</span>ail
        <small>.</small>
        com<br />
      </p>
      <h2>Haftungsausschluss:</h2>
      <h3>Haftung für Inhalte</h3>
      <p>
        Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. Für die Richtigkeit, Vollständigkeit
        und Aktualität der Inhalte können wir jedoch keine Gewähr übernehmen. Als Diensteanbieter sind wir
        gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich.
        Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder
        gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine
        rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von
        Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist
        jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden
        von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.
      </p>
      <h3>Haftung für Links</h3>
      <p>
        Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss
        haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der
        verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die
        verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft.
        Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche
        Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht
        zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.
      </p>
      <h3>Urheberrecht</h3>
      <p>
        Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen
        Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der
        Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw.
        Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch
        gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die
        Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten
        Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden
        Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.
      </p>
      <h3>Datenschutz</h3>
      <p>
        Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf
        unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen) erhoben
        werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre
        ausdrückliche Zustimmung nicht an Dritte weitergegeben. <br />
        Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail)
        Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist
        nicht möglich. <br />
        Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur
        Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit
        ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im
        Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.
      </p>
      <h2>Google Analytics</h2>
      <p>
        Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. (&quot;Google&quot;).
        Google Analytics verwendet sog. &quot;Cookies&quot;, Textdateien, die auf Ihrem Computer gespeichert
        werden und die eine Analyse der Benutzung der Website durch Sie ermöglicht. Die durch den Cookie
        erzeugten Informationen über Ihre Benutzung diese Website (einschließlich Ihrer IP-Adresse) wird an
        einen Server von Google in den USA übertragen und dort gespeichert. Google wird diese Informationen
        benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten für die
        Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung
        verbundene Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an
        Dritte übertragen, sofern dies gesetzlich vorgeschrieben oder soweit Dritte diese Daten im Auftrag von
        Google verarbeiten. Google wird in keinem Fall Ihre IP-Adresse mit anderen Daten der Google in
        Verbindung bringen. Sie können die Installation der Cookies durch eine entsprechende Einstellung Ihrer
        Browser Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls
        nicht sämtliche Funktionen dieser Website voll umfänglich nutzen können. Durch die Nutzung dieser
        Website erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten durch Google in der zuvor
        beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden.
      </p>
      <h2>Google AdSense</h2>
      <p>
        Diese Website benutzt Google Adsense, einen Webanzeigendienst der Google Inc., USA
        (&quot;Google&quot;). Google Adsense verwendet sog. &quot;Cookies&quot; (Textdateien), die auf Ihrem
        Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglicht.
        Google Adsense verwendet auch sog. &quot;Web Beacons&quot; (kleine unsichtbare Grafiken) zur Sammlung
        von Informationen. Durch die Verwendung des Web Beacons können einfache Aktionen wie der
        Besucherverkehr auf der Webseite aufgezeichnet und gesammelt werden. Die durch den Cookie und/oder Web
        Beacon erzeugten Informationen über Ihre Benutzung diese Website (einschließlich Ihrer IP-Adresse)
        werden an einen Server von Google in den USA übertragen und dort gespeichert. Google wird diese
        Informationen benutzen, um Ihre Nutzung der Website im Hinblick auf die Anzeigen auszuwerten, um
        Reports über die Websiteaktivitäten und Anzeigen für die Websitebetreiber zusammenzustellen und um
        weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch
        wird Google diese Informationen gegebenenfalls an Dritte übertragen, sofern dies gesetzlich
        vorgeschrieben oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Google wird in keinem
        Fall Ihre IP-Adresse mit anderen Daten der Google in Verbindung bringen. Das Speichern von Cookies auf
        Ihrer Festplatte und die Anzeige von Web Beacons können Sie verhindern, indem Sie in Ihren
        Browser-Einstellungen &quot;keine Cookies akzeptieren&quot; wählen (Im MS Internet-Explorer unter
        &quot;Extras &gt; Internetoptionen &gt; Datenschutz &gt; Einstellung&quot;; im Firefox unter
        &quot;Extras &gt; Einstellungen &gt; Datenschutz &gt; Cookies&quot;); wir weisen Sie jedoch darauf
        hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website voll umfänglich
        nutzen können. Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie
        erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck
        einverstanden.
      </p>

      <h2>Datenschutzerklärung für die Nutzung von YouTube</h2>
      <p>
        Unsere Webseite nutzt Plugins der von Google betriebenen Seite YouTube. Betreiber der Seiten ist die
        YouTube, LLC, 901 Cherry Ave., San Bruno, CA 94066, USA. Wenn Sie eine unserer mit einem
        YouTube-Plugin ausgestatteten Seiten besuchen, wird eine Verbindung zu den Servern von YouTube
        hergestellt. Dabei wird dem Youtube-Server mitgeteilt, welche unserer Seiten Sie besucht haben.
      </p>
      <p>
        Wenn Sie in Ihrem YouTube-Account eingeloggt sind ermöglichen Sie YouTube, Ihr Surfverhalten direkt
        Ihrem persönlichen Profil zuzuordnen. Dies können Sie verhindern, indem Sie sich aus Ihrem
        YouTube-Account ausloggen.
      </p>
      <p>
        Weitere Informationen zum Umgang von Nutzerdaten finden Sie in der Datenschutzerklärung von YouTube
        unter
        <a href="https://www.google.de/intl/de/policies/privacy">
          https://www.google.de/intl/de/policies/privacy
        </a>
      </p>
      <p />
      <p>
        Das Impressum dieser Homepage wurde zum Teil mit dem{' '}
        <a href="http://www.impressum-generator.de/">Impressum Generator</a> der{' '}
        <a href="http://www.kanzlei-hasselbach.de">Kanzlei Hasselbach</a> erstellt.
      </p>

      <BackButton />
    </article>
  );
}
