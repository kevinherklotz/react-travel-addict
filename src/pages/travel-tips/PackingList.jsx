import React from 'react';
import BackButton from 'components/BackButton';
import { Link } from 'react-router-dom';

/**
 * PackingList - content of page "Packliste"
 *
 * @returns {JSX} - JSX that will be injected
 */
export default function PackingList() {
  return (
    <div className="container pt-3">
      <article>
        <h1>Packliste</h1>
        <p>
          Ich habe mir Mühe gegeben, alles mögliche aufzuführen, solltet ihr noch Hinweise oder
          Ergänzungen haben, dann schreibt mir doch einfach bei
          {' '}<a href="https://www.facebook.com/real.travel.addict/">Facebook</a> und ich versuche die
          Liste dann zu ergänzen und somit zu perfektionieren.
        </p>
        <p>
          Bedenkt, dass diese Liste eventuell mehr beinhaltet als ihr mitnehmen könnt. Letztendlich
          entscheidet ihr, was ihr mitnehmt. Ich habe immer den Fehler gemacht zu viel mitzunehmen - viele
          kleinere Dinge und vor allem auch Klamotten kann man sich ganz einfach dazu kaufen und sind im
          Ausland oft auch günstiger als in Deutschland. Bei Technik habe ich hingegen die Erfahrung
          gemacht, dass sie in Kanada und Neuseeland viel teurer ist; in Australien und den USA hingegen
          oft günstiger.
        </p>

        großer Rucksack 65-85 Liter
        Tagesrucksack

        <h3>Technik</h3>
        <div className="table-responsive">
          <table className="table table-bordered table-striped table-ruled">
            <tbody>
              <tr>
                <td>Smartphone
                  <small> (das gute Fotos macht)</small>
                </td>
                <td>
                  Sehr wichtig, wie ich finde, denn oft ergibt sich eine Fotomöglichkeit, bei der
                  man nur sein Smartphone dabei hat.<br />
                  Klar ist das iPhone dafür nicht schlecht, aber schaut euch auch mal das viel
                  günstigere <a href="http://amzn.to/2jm5QLi">LG G4</a> an
                  (<a href="https://www.youtube.com/watch?v=R1ED1_4DS4E">krasses Vergleichsvideo</a>),
                  mit dem ich seeehr zufrieden bin. Diese 2 Fotos (
                  <a href="https://www.facebook.com/trvladdct/photos/a.597284810461556.1073741833.591651861024851/597285333794837">Link 1 </a>,{' '}
                  <a href="https://www.facebook.com/trvladdct/photos/a.597284810461556.1073741833.591651861024851/597285007128203">Link 2 </a>){' '}
                  {' '}habe ich zum Beispiel mit dem LG G4 gemacht.
                  Findet man mittlerweile auch gebraucht ganz günstig ;-)
                  <br /><strong>Wichtig: </strong> Achtet darauf, dass das Smartphone kein SIM-Lock
                  hat, falls ihr eine SIM-Karte im Ausland nutzen wollt!
                </td>
              </tr>
              <tr>
                <td>Kamera</td>
                <td>
                  Ich habe eine fette Spiegelreflexkamera mit, weil ich das Fotografieren so mag,
                  aber für die meisten reicht wahrscheinlich eine Kompaktkamera aus.
                  <br />
                  Eine zu empfehlen ist sehr schwierig, da ich dein Budget und Anforderungen nicht
                  kenne.
                  <br />
                  Aber ich kann zumindest diese nicht ganz so kompakte Kamera sehr empfehlen:{' '}
                  <a href="http://amzn.to/2k5WEwA">Panasonic Lumix DMC-FZ200</a>.
                </td>
              </tr>
              <tr>
                <td>Objektive</td>
                <td><a href="http://amzn.to/2sbVdif">Walimex Pro 8mm 1:3,5</a></td>
              </tr>
              <tr>
                <td>Stativ</td>
                <td>
                  Klar brauch nicht jeder ein Stativ, aber für diejenigen, die eins mitnehmen wollen,
                  kann ich das leichte und hochwertige <a href="http://amzn.to/2iOsRbl">SIRUI T-005X</a>
                  {' '}empfehlen - ich bin sehr zufrieden damit. Eine kleinere und flexible Alternative
                  dazu ist der <a href="http://amzn.to/2jmAzbr">Jobi Gorillapod</a>.
                </td>
              </tr>
              <tr>
                <td>Action-Kamera</td>
                <td>
                  Viele denken da an GoPro, aber ich kann euch sagen, dass die Bildqualität von
                  der günstigeren <a href="http://amzn.to/2jxcxwd">Sony FDR-X1000 4K Actioncam</a>
                  {' '}viel besser ist, als die von der <a href="http://amzn.to/2jmhmGG">GoPro HERO4</a>
                  {' '}(<a href="https://youtu.be/CfHy4eASLzo?t=9m37s">Beweis</a>).
                  Auch bei den jeweils neuen Modellen der Hersteller muss man sagen, dass die
                  {' '}<a href="http://amzn.to/2riwTxV">Sony FDR-X3000</a> die
                  <a href="http://amzn.to/2iNuBkZ">Gopro HERO5</a> in Sachen Bildqualität deutlich
                  schlägt.
                </td>
              </tr>
              <tr>
                <td>Laptop / Tablet</td>
                <td>
                  Für mich ist ein Laptop natürlich unverzichtbar, aber für viele reicht auch ein
                  Tablet. Das müsst ihr selbst entscheiden, aber bedenkt auch, wo ihr die ganzen
                  Fotos eurer Kamera (oder oft auch die von anderen) speichern werdet.
                  <br />
                  Mir persönlich gefallen übrigens die
                  {' '}<a href="http://amzn.to/2m9L166">Filz-Schutzhüllen von Inateck</a>.
                </td>
              </tr>
              <tr>
                <td>externe Festplatte</td>
                <td>
                  Nicht unbeding nötig, aber wenn man vor hat viele Fotos und Videos zu machen
                  oder Filme zu schauen, bietet das eine praktische Speicherplatzergänzung.
                </td>
              </tr>
              <tr>
                <td>USB-Stick</td>
                <td>
                  Wird einfach immer mal wieder benötigt, selbst wenn man gar keinen Laptop dabei
                  hat. Mittlerweile gibt es sogar ganz kleine mit 128GB für um die 30€, wie zum
                  Beispiel <a href="http://amzn.to/2jm94hO">diesen hier</a>. Damit könnte man
                  schon fast eine externe Festplatte ersetzen.
                </td>
              </tr>
              <tr>
                <td>Power Bank</td>
                <td>
                  Ganz wichtig! Ich lade mein Smartphone, meine Spiegelreflex, meine Actioncam, meine
                  Lautsprecher und mein Kindle damit, wenn ich unterwegs bin. Mehr als 15.000 mAh habe
                  ich dabei nie gebraucht, da man ja auch nicht immer alles laden muss. Oft findet man
                  Steckdosen, so dass man nicht mehr brauchen sollte.{' '}
                  <a href="http://amzn.to/2jxMQMc">Diese EasyAcc Powerbank hier</a> ist sehr gut!
                  <br />
                  Eine mit Solarpanel kann ich übrigens nicht empfehlen, da diese 2 volle Tage Sonne
                  brauchen, um ein Handy zu laden.
                </td>
              </tr>
              <tr>
                <td>Stirnlampe</td>
                <td>
                  Höhlen erforschen, wandern vor Sonneaufgang, nachts auf dem Campingplatz aufs
                  Klo gehen - ihr werdet sie mit Sicherheit brauchen! Ich selbst habe diese hier -
                  sie ist sehr hell, hat aber auch einen relativ dunklen (roten) Modus, was
                  besonders nachts angenehm ist, wenn die Augen noch lichtempfindlich sind. Wir
                  konnten damit sogar die lichtscheuen Kiwis in Neuseeland beobachten :-)
                </td>
              </tr>
              <tr>
                <td>Musicman</td>
                <td>
                  Von den kompakten Musikboxen ist der <a href="http://amzn.to/2iPaByk">Musicman</a>
                  meiner Meinung nach die Beste und für ~15€ auch sehr preiswert. Und ich habe
                  schon viele verschiedene ausprobiert. Es gibt auch eine{' '}
                  <a href="http://amzn.to/2iPeOSC">Bluetooth Variante</a> davon. Der Akku hält bei
                  mir um die 6 Stunden.
                </td>
              </tr>
              <tr>
                <td>Kindle</td>
                <td>
                  Bücher sind einfach zu schwer zum Reisen, deswegen bietet sich da ein
                  {' '}<a href="http://amzn.to/2iHj8QH">Kindle</a> an. Das Konkurrenzprodukt{' '}
                  <a href="http://mytolino.de/tolino-ebook-reader-vergleich/">Tolino</a> soll wohl
                  auch ganz gut sein.
                  <br />
                  Wer analoges Lesen mehr mag, der findet auch viele
                  gebrauchte Bücher für 2 Dollar oder so im jeweiligen Reiseland.
                </td>
              </tr>
              <tr>
                <td>Kopfhörer</td>
                <td />
              </tr>
              <tr>
                <td>Steckdosenadapter</td>
                <td>
                  Ich kann <a href="http://amzn.to/2jCAmEf">diesen hier</a> empfehlen, da er für
                  alle Länder ist und noch 2 USB-Slots hat.
                </td>
              </tr>
              <tr>
                <td>kompakter Steckdosenverteiler</td>
                <td>
                  Manchmal nützlich, wenn in der Bibliothek schon andere Europäer die Steckplätze belegen.
                </td>
              </tr>
              <tr>
                <td>KFZ-USB-Adapter</td>
                <td>
                  Solltet ihr vorhaben, euch vor Ort ein Auto zu kaufen/mieten, ist auch so ein
                  {' '}<a href="http://amzn.to/2jvWTBF">kleiner Adapter hier</a> ein Muss.
                </td>
              </tr>
              <tr>
                <td colSpan="2">
                  Zu guterletzt: Vergesst eure Ladegeräte nicht, steckt euch lieber ein USB-Kabel
                  mehr ein und denkt auch an genügend große Speicherkarten (und evtl. Adapter) für
                  eure Kamera ;-)
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        Ersatzakkus in Flugzegen? //todo


        <h3>Klamotten</h3>
        <p>
          Beachtet, dass diese Liste aus der Sicht eines Mannes geschrieben wurde und deswegen so etwas
          wie Bikini, Leggings, Rock/Kleid oder Top hier nicht aufgelistet ist.
          <br />
          Das meiste gilt wohl für beide Geschlechter, aber die Frauen unter euch wissen wohl eher, was
          sie zusätzlich noch brauchen.
        </p>
        <div className="table-responsive">
          <table className="table table-bordered table-striped table-ruled">
            <tbody>
              <tr>
                <td>1 lange Hose</td>
                <td>Reicht in Verbindung mit der Zip-Off Hose aus.</td>
              </tr>
              <tr>
                <td>1 Zip-Off Hose</td>
                <td>Sehr praktisch, vor allem beim Wandern und beim Flug.</td>
              </tr>
              <tr>
                <td>2 kurze Hosen</td>
                <td>Eine gute und eine alte zum arbeiten.</td>
              </tr>
              <tr>
                <td>2 Badehosen</td>
                <td>Braucht kaum Platz und kann auch als kurze Alltagshose dienen.</td>
              </tr>
              <tr>
                <td>6 T-Shirts</td>
                <td>
                  5 gute und eins zum Arbeiten reichen völlig aus. Ansonsten gibt es vor Ort meist
                  auch günstige Klamotten.
                </td>
              </tr>
              <tr>
                <td>1 Unterhemd</td>
                <td>Falls es mal kalt wird.</td>
              </tr>
              <tr>
                <td>2 Pullover</td>
                <td>Ein guter (am besten mit Kaputze) und evtl. ein alter zum Arbeiten.</td>
              </tr>
              <tr>
                <td>1 gute, regendichte Jacke</td>
                <td>
                  Am besten eine wie diese hier //todo mit 2 Schichten, die ist bei Kälte gut und
                  auch für warme Regentage. Gebt bei einer Jacke lieber etwas mehr aus.
                </td>
              </tr>
              <tr>
                <td>8 Unterhosen</td>
                <td>Passt ganz gut, da ich so ca. aller 1 Woche Wäsche gewaschen habe.</td>
              </tr>
              <tr>
                <td>12 Socken</td>
                <td>
                  Ich habe 7 lange und 5 kurze und komme damit gut hin. Socken nehmen ja auch kaum
                  Platz weg.
                  <br />
                  Wer Wanderschuhe mitnimmt, sollte auch nicht an guten Wandersocken sparen. Ich
                  habe mir <a href="http://amzn.to/2lWabJi">diese hier von Falke</a>
                  {' '}gegönnt und es nicht bereut, da ich mit denen viel weniger Blasen bekomme.
                </td>
              </tr>
              <tr>
                <td>Kappe</td>
                <td />
              </tr>
              <tr>
                <td>Mütze</td>
                <td>für kalte Nächte</td>
              </tr>
              <tr>
                <td>Handschuhe</td>
                <td>
                  Für Neuseeland und Kanada sehr sinnvoll, für wärmere Länder vielleicht nicht unbedingt.
                </td>
              </tr>
              <tr>
                <td>Schal/Halstuch</td>
                <td>Ich selbst habe so einen{' '}
                  <a href="http://amzn.to/2lWbvMp">Schlauchschal</a>, der mir bisher sehr nützlich war.
                </td>
              </tr>
              <tr>
                <td>Gürtel</td>
                <td />
              </tr>
              <tr>
                <td>Wanderschuhe</td>
                <td>Sollte man während des Fluges tragen, um Platz im Rucksack zu sparen.</td>
              </tr>
              <tr>
                <td>Turnschuhe</td>
                <td />
              </tr>
              <tr>
                <td>evtl. alte Arbeitsschuhe</td>
                <td>Oder einfach die Wanderschuhe zum Arbeiten benutzen.</td>
              </tr>
              <tr>
                <td>FlipFlops/Badelatschen</td>
                <td />
              </tr>
            </tbody>
          </table>
        </div>


        <h3>Sonstiges</h3>
        <div className="table-responsive">
          <table className="table table-bordered table-striped table-ruled">
            <tbody>
              <tr>
                <td>Reiseführer</td>
                <td>
                  Nicht unbedingt nötig, wird auch oftmals vor Ort gebraucht verkauft.
                  Eine Alternative wäre auch ein Reiseführer als App (siehe{' '}
                  <Link to="/reisetipps/apps" title="Nützliche Apps">hier</Link>)
                </td>
              </tr>
              <tr>
                <td>Plastikdosen</td>
                <td>
                  Ich habe 3 verschiedene Größen mitgenommen und sie sind sehr nützlich für die
                  Aufbewahrung von Essen und auch Kleinkram.
                </td>
              </tr>
              <tr>
                <td>Taschentücher</td>
                <td>Habe in Kanada zum Beispiel keine 4-lagigen Taschentücher gefunden.</td>
              </tr>
              <tr>
                <td>Taschenmesser</td>
                <td>
                  Nicht ins Handgepäck! Könnte man eigentlich auch zu Hause lassen - ich habe
                  meins so gut wie nie benutzt.
                </td>
              </tr>
              <tr>
                <td>Besteck</td>
                <td>1 scharfes Messer, eine Gabel, ein Löffel reichen. Auch nicht ins Handgepäck!
                </td>
              </tr>
              <tr>
                <td>Duct Tape / Panzertape</td>
                <td>Braucht man ständig mal.</td>
              </tr>
              <tr>
                <td>Ohrenstöpsel</td>
                <td>Gut in Hostels.</td>
              </tr>
              <tr>
                <td>kleines Schloss.</td>
                <td>Manchmal kann man damit in Hostels seine Sachen abschließen.</td>
              </tr>
              <tr>
                <td>Sonnenbrille</td>
                <td>(+ Etui)</td>
              </tr>
              <tr>
                <td>Wäscheleine</td>
                <td>Sehr nützlich, nicht nur für Wäsche! Evtl. noch ein paar Klammern mitnehmen.
                </td>
              </tr>
              <tr>
                <td>Kugelschreiber + kleiner Block</td>
                <td />
              </tr>
              <tr>
                <td>Mini-Luftpumpe</td>
                <td>
                  Klingt zwar überflüssig, aber ich habe{' '}
                  <a href="http://amzn.to/2jFoYCO">meine Universalpumpe</a> schon auf so manchen
                  Radtouren oder für Volleybälle und Fußbälle gebraucht.
                </td>
              </tr>
              <tr>
                <td><a href="http://amzn.to/2kTTgXt">Mikrofaser Badetuch</a></td>
                <td>Mikrofaser trocknet einfach viel schneller.</td>
              </tr>
              <tr>
                <td>kleines Handtuch</td>
                <td>Meiner Meinung nach nicht mal unbedingt nötig.</td>
              </tr>
              <tr>
                <td>Rucksack Regenschutz</td>
                <td>
                  Einige Rucksäcke haben das bereits integriert, kann man aber auch in jeder Größe
                  {' '}<a href="http://amzn.to/2kjtA3h">nachkaufen</a>.
                </td>
              </tr>
              <tr>
                <td><a href="http://amzn.to/2lZ9kUX">aufblasbares Nackenkissen</a></td>
                <td>Fürs Flugzeug oder zum Zelten gehen ganz nützlich</td>
              </tr>
              <tr>
                <td>Kartenspiel</td>
                <td>Gut für Regentage.</td>
              </tr>
              <tr>
                <td>Plastikbeutel</td>
                <td>Gut als Müllbeutel, für nasse Wäsche und und und.</td>
              </tr>
              <tr>
                <td>Karabinerhaken</td>
                <td>
                  Damit hänge ich zum Beispiel immer meine Schuhe an den Rucksack, wenn ich am
                  Strand bin.
                </td>
              </tr>
              <tr>
                <td>Alkohol</td>
                <td>
                  Solltet ihr noch Platz in eurem Rucksack/Koffer haben, dann solltet ihr
                  vielleicht auch in Erwägung ziehen, ein paar Flaschen Hochprozentiges
                  mitzunehmen. In fast jedem Land ist Alkohol deutlich teurer als in Deutschland,
                  zum Beispiel kostet Schnaps in Neuseeland ca. das 5-fache. Hier gibt es also
                  großes Sparpotenzial. Checkt aber vorher nochmal die Einfuhrbestimmungen des
                  jeweiligen Landes (und evtl. auch Zwischenstops). Hier ein paar Beispiele (ohne
                  Gewähr):
                  <dl>
                    <dt>Kanada:</dt>
                    <dd>--todo</dd>
                    <dt>Neuseeland:</dt>
                    <dd>max. 3 Flaschen á 0,7 Liter //todo</dd>
                    <dt>Australien:</dt>
                    <dd>--todo</dd>
                  </dl>
                </td>
              </tr>
              <tr>
                <td>Klopapier</td>
                <td>
                  Muss ja keine volle Rolle sein, aber davon sollte man immer etwas in seinem
                  Rucksack haben!
                </td>
              </tr>
            </tbody>
          </table>
        </div>


        <h3>Hygiene und Medizin</h3>
        <p>
          Ich habe einfach mal alles aufgeführt, was mir so einfällt, aber besonders bei dieser Liste
          variiert das ja von Person zu Person.
        </p>
        <div className="table-responsive">
          <table className="table table-bordered table-striped table-ruled">
            <tbody>
              <tr>
                <td>Kulturbeutel</td>
                <td>
                  Am besten <a href="http://amzn.to/2lWf7hc">zum Aufhängen</a>.
                </td>
              </tr>
              <tr>
                <td>Rasierer</td>
                <td />
              </tr>
              <tr>
                <td>Zahnbürste + Zahnpasta</td>
                <td />
              </tr>
              <tr>
                <td>Zahnseide</td>
                <td />
              </tr>
              <tr>
                <td>Duschbad/Schampoo</td>
                <td />
              </tr>
              <tr>
                <td>Deo/Parfum</td>
                <td />
              </tr>
              <tr>
                <td>Nagelschere</td>
                <td />
              </tr>
              <tr>
                <td>Pinzette</td>
                <td />
              </tr>
              <tr>
                <td>Haargel</td>
                <td />
              </tr>
              <tr>
                <td>Schminkzeug/Make-Up</td>
                <td>
                  Lieber nicht zu viel, die meisten anderen Backpackerinnen schminken sich auch nicht ;-)
                </td>
              </tr>
              <tr>
                <td>Kontaktlinsen</td>
                <td>(+ Flüssigkeit)</td>
              </tr>
              <tr>
                <td>Haargel</td>
                <td />
              </tr>
              <tr>
                <td>Pille/Kondome</td>
                <td />
              </tr>
              <tr>
                <td>Tampons</td>
                <td />
              </tr>
              <tr>
                <td>Schlaftabletten</td>
                <td />
              </tr>
              <tr>
                <td>Allergietabletten</td>
                <td />
              </tr>
              <tr>
                <td>Sonnencreme 30+</td>
                <td />
              </tr>
              <tr>
                <td>Fieber-/Schmerzmittel</td>
                <td>Paracetamol oder so</td>
              </tr>
              <tr>
                <td>Magnesiumtabletten</td>
                <td>
                  Helfen gut bei längeren Wanderungen oder übermäßigem Alkoholkonsum. Am besten
                  vorher nehmen ;-)
                </td>
              </tr>
              <tr>
                <td>individuell benötigte Medikamente</td>
                <td />
              </tr>
              <tr>
                <td>Pflaster</td>
                <td />
              </tr>
            </tbody>
          </table>
        </div>

        <h3>Dokumente</h3>
        <div className="table-responsive">
          <table className="table table-bordered table-striped table-ruled">
            <tbody>
              <tr>
                <td>Dokumentenmappe</td>
                <td>
                  Ich kann euch eine Dokumentenmappe wie <a href="http://amzn.to/2jXS94M">diese hier</a>{' '}
                  wärmstens empfehlen, um die Übersicht zu behalten. Gibt es so ähnlich
                  im Pfennigpfeiffer auch günstiger.
                </td>
              </tr>
              <tr>
                <td>EU-Führerschein + internationaler Führerschein</td>
                <td />
              </tr>
              <tr>
                <td>Reisepass</td>
                <td>achtet darauf, dass er nicht bald abläuft.</td>
              </tr>
              <tr>
                <td>Visum + Zahlungsbestätigung</td>
                <td />
              </tr>
              <tr>
                <td>Impfausweis</td>
                <td>(am besten einen internationalen)</td>
              </tr>
              <tr>
                <td>Krankenversicherungsbestätigung</td>
                <td>
                  Auch in Englisch. Ohne dieses Dokument wird einem in Kanada die Einreise
                  verweigert, in Neuseeland ist es allerdings keine Pflicht.
                </td>
              </tr>
              <tr>
                <td>Flugticket</td>
                <td>Meist bekommt man das ja eh erst am Flughafen.</td>
              </tr>
              <tr>
                <td>Kreditkarte</td>
                <td>
                  Ich habe eine DKB-Kreditkarte und kann damit kostenlos im Ausland Bargeld
                  abheben - Bargeld mitzunehmen war für mich daher unnötig.
                </td>
              </tr>
              <tr>
                <td>aktueller Kontoauszug</td>
                <td>
                  Musste ich in Kanada und auch in Neuseeland vorzeigen, um nachzuweisen, dass ich
                  genügend Geld für einen Rückflug habe, da ich nur mit einem One-way-Ticket
                  eingereist bin. Die Grenzbeamten entscheiden dann meist nach eigenem Ermessen,
                  ob das ausreicht - man findet unterschiedliche Angaben darüber, aber ich sag mal
                  so unter 3000 Euro sollte es für einen 6-monatigen Aufenthalt zum Beispiel nicht sein.
                </td>
              </tr>
              <tr>
                <td>Nummern und Adressen von abgeschlossenen Versicherungen, Banken, ...</td>
                <td>
                  Manchmal braucht man das, wenn man mit irgendwelchen Behörden kommunizieren
                  muss. Man kann natürlich auch seine ganzen Ordner zu Hause bei seinen Eltern
                  lassen und die dann fragen.
                  <br />
                  Die Notfall-Nummer für die Krankenversicherung macht
                  sich ganz nützlich auf einem Zettel im Portemonnaie.
                </td>
              </tr>
              <tr>
                <td>TAN-Liste/TAN-Generator</td>
                <td>
                  Wichtig fürs Online-Banking.
                </td>
              </tr>
              <tr>
                <td>Lebenslauf (auf englisch)</td>
                <td>Kommt besser, wenn man den schon in Deutschland vorbereitet.</td>
              </tr>
              <tr>
                <td>(Arbeits-)zeugnisse</td>
                <td>
                  Eine digitale Kopie ist hier vielleicht am sinnvollsten. Wenn man nur auf
                  Farmen, in Bars etc. arbeiten möchte, braucht man das nicht. Falls man
                  in seinem alten Beruf tätig sein möchte, kann das ganz nützlich sein.
                  <br />
                  Auch um sich vom Ausland aus in Deutschland zu bewerben, ist es sehr hilfreich,
                  wenn man das bereits eingescannt hat - die meisten denken gar nicht so weit voraus.
                </td>
              </tr>
              <tr>
                <td>Passbilder</td>
                <td>
                  Nicht unbedingt nötig, kann aber nützlich sein. Ich brauchte in Kanada auch
                  schon mal 2 Passbilder von mir.
                </td>
              </tr>
              <tr>
                <td>Adresse der ersten Unterkunft</td>
                <td>Wollen die oft bei der Einreise wissen.</td>
              </tr>
              <tr>
                <td>leerer Block</td>
                <td />
              </tr>
            </tbody>
          </table>
        </div>


        <h3>Überflüssiges</h3>
        <p>
          Alles, was ihr <strong>nicht</strong> braucht.
        </p>
        <div className="table-responsive">
          <table className="table table-bordered table-striped table-ruled">
            <tbody>
              <tr>
                <td>Regenschirm</td>
                <td>Nimmt zu viel Platz weg und dafür gibt es ja die Regenjacke.</td>
              </tr>
              <tr>
                <td>Schlafsack, Zelt oder andere Campingausrüstung</td>
                <td>Viel zu groß! Kann vor Ort oft günstig gebraucht gekauft werden.</td>
              </tr>
              <tr>
                <td>Insektenschutzmittel</td>
                <td>Lieber vor Ort kaufen, da diese meist besser wirken.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </article>
      <BackButton path="/reisetipps" />
    </div>
  );
}
