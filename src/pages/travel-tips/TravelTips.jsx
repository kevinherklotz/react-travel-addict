import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import BackButton from 'components/BackButton';

/**
 * Travel Tips - root page of the different travel tips that will be shown in a list
 *
 * @param {Object} props - The properties that react-router gives as parameter
 * @param {Object} props.match - Contains information about how a <Route path> matched the URL
 * @param {string} props.match.url - The path pattern used to match
 * @returns {JSX} - JSX that will be injected
 */
export default function TravelTips({ match }) {
  return (
    <div className="container pt-3">

      <article>
        <h1>Reisetipps</h1>
        {/* TODO */}
        <pre>
          {JSON.stringify(match, null, 2)}
        </pre>
        <p>
          Hier findest du ein paar allgemeine Tipps, die dir bei deiner Reise helfen könnten.
        </p>
      </article>


      <div className="row my-4">
        <div className="col-xs-12 col-md-6 col-lg-4">
          <Link to="/reisetipps/packliste" className="no-anchor-style">
            <div className="shadow hover-scale px-3 py-4">
              <div className="row text-center">
                <div className="col-xs-12 mb-3">
                  <span className="fa fa-list fa-4x" />{' '}
                </div>
                <div className="col-xs-12">
                  <h4>Packliste</h4>
                </div>
              </div>
            </div>
          </Link>
        </div>
        <div className="col-xs-12 col-md-6 col-lg-4">
          <Link to="/reisetipps/vorbereitungen" className="no-anchor-style">
            <div className="shadow hover-scale px-3 py-4">
              <div className="row text-center">
                <div className="col-xs-12 mb-3">
                  <span className="fa fa-suitcase fa-4x" />{' '}
                </div>
                <div className="col-xs-12">
                  <h4>Vorbereitungen</h4>
                </div>
              </div>
            </div>
          </Link>
        </div>
        <div className="col-xs-12 col-md-6 col-lg-4">
          <Link to="/reisetipps/apps" className="no-anchor-style">
            <div className="shadow hover-scale px-3 py-4">
              <div className="row text-center">
                <div className="col-xs-12 mb-3">
                  <span className="fa fa-mobile fa-4x" />{' '}
                </div>
                <div className="col-xs-12">
                  <h4>Nützliche Apps</h4>
                </div>
              </div>
            </div>
          </Link>
        </div>
      </div>

      <BackButton />
    </div>
  );
}

TravelTips.propTypes = {
  match: PropTypes.shape({
    url: PropTypes.string.isRequired,
  }).isRequired,
};
