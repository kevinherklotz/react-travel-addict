import React from 'react';
import BackButton from 'components/BackButton';

/**
 * UsefulApps - content page "Nützliche Apps"
 *
 * @returns {JSX} - JSX that will be injected
 */
export default function UsefulApps() {
  return (
    <div className="container pt-3">
      <article>
        <h1>Nützliche Apps</h1>
        <h3>Folgende Apps haben mir auf meinen Reisen sehr weitergeholfen:</h3>

        <h4>1. Wörterbuch-App</h4>
        <p>
          Ab und zu braucht man da doch mal ein Wörterbuch und da ist es ganz nützlich, wenn man eine App hat,
          die auch offline übersetzen kann. Ich habe mal dict.cc (todo: Link) genutzt, da die App aber zu oft
          abgestürzt ist bin ich zu Linguee (todo: Link) gewechselt - bei beiden kann man umfangreiche
          Wörterbücher in verschiedensten Sprachen kostenlos herunterladen.
        </p>

        <h4>2. Tagebuch-App</h4>
        <p>
          Ja auch wenn viele auf handgeschriebene Reisetagebücher schwören, kann ich Journey nur jedem sehr
          empfehlen. Die Vorteile gegenüber einem Handgeschriebenen Tagebuch sind:
        </p>
        <ul>
          <li>
            <strong>Fotos:</strong> Zu jedem Eintrag kann man ein oder mehrere Fotos hinzufügen
          </li>
          <li>
            <strong>Standort:</strong> Es wird automatisch der Standort hinzugefügt, wo man den Eintrag macht
            oder der Aufnahmeort des Fotos. Man kann also später auf einer Karte sehen, wo welcher Eintrag
            gemacht wurde.
          </li>
          <li>
            <strong>Erinnerungen:</strong> Man hat die Option sich zu einer beliebigen Zeit erinnern zu
            lassen, dass man den Eintrag schreiben sollte.
          </li>
          <li>
            <strong>Backups:</strong> Man kann auch (verschlüsselte) Backups in der Cloud aktivieren. Das
            heißt, wenn dein Smartphone abhanden kommt, verlierst du trotzdem keinen deiner Tagebuch-Einträge.
          </li>
        </ul>

        <h4>3. Navigations-App</h4>
        <p>
          Ganz klassisch hierfür nutze ich Google Maps. Was viele nicht wissen ist, dass man sich einfach
          Offline-Karten herunterladen kann, um auch ohne Internet navigieren zu können. Dazu Einfach ...todo
        </p>
        <p>
          Als Alternative könnte man auch MapsMe, todo oder todo nehmen, welche auf Karten von{' '}
          <abbr title="Open Street Map">OSM</abbr> aufbauen, welche aber nicht immer ganz richtig liegen mit
          der Navigation, was aber verschmerzbar ist, dafür dass es kostenlos ist.
        </p>

        <h4>4. Sprachlern-App</h4>
        <p>
          Falls du dich noch nicht sicher in einer Sprache fühlst oder vielleicht eine neue Sprache erlernen
          möchtest, kann ich Memrise (todo) empfehlen. Man kann da ganz verschiedene Sprachpakete
          herunterladen - von Anfänger bis Profi.
          <br />
          Ich lerne damit zum Beispiel &quot;Idioms, Sayings and Slang&quot; in Englisch und ich habe auch den
          Einsteigerkurs für Spanisch angefangen, da ich spanisch lernen möchte.
          <br />
          Leider funktioniert das nur online und wenn man es auch offline benutzen möchte, muss man ca. 35
          Euro im Jahr bezahlen. Mir war es das wert, da man da lesen, schreiben und hören in der jeweiligen
          Sprache lernt, sogar mit Sprachbeispielen von verschiedenen Einheimischen.
          <br />
          Eine oft genutzte Alternative dazu ist todo.
        </p>

        <h4>5. Reiseführer-App</h4>
        <p>
          Anstatt sich zum Beispiel einen Lonely Planet zu kaufen, könnte man sich auch die App (todo) für den
          gleichen Preis herunterladen. Das spart auf alle Fälle Gewicht.
        </p>

        <h4>6. Landessprezifische Apps</h4>
        <p>Campermate und Wikicamps</p>
      </article>

      <BackButton path="/reisetipps" />
    </div>
  );
}
