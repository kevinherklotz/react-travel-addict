const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const autoprefixer = require('autoprefixer');
const StyleLintPlugin = require('stylelint-webpack-plugin');

// Plugins

/**
 * Generate the array with plugins based on the current mode (development or production)
 * @param {string} mode  - mode that webpack is running with: 'development' or 'production'
 * @returns {Array} - array with plugins that will be used by webpack
 */
const getPlugins = (mode) => {
  // #### Config ####

  // all default plugins used for development and production
  const defaultPlugins = [
    new HtmlWebPackPlugin({
      template: './src/index.html',
      filename: './index.html',
    }),
    new StyleLintPlugin({
      files: 'src/**/*.scss',
      emitErrors: false && mode === 'production', // for some reason this value determines if webpack fails on error
      failOnError: false, // for some reason this value determines if webpack emits errors
      syntax: 'scss',
    }),
  ];

  // Plugins only for production
  const productionPlugins = [
    // is needed to read css files (only in development mode)
    new MiniCssExtractPlugin(),
    // generates stats.json file for bundle analyzer dashboard
    new BundleAnalyzerPlugin({
      generateStatsFile: true,
      analyzerMode: 'disabled',
      defaultSizes: 'gzip',
    }),
    // minifies CSS, uses cssnano by default
    new OptimizeCssAssetsPlugin(),
    new CleanWebpackPlugin('dist/*'),
  ];

  // Plugins only for development
  const developmentPlugins = [];

  // #### Logic ####
  let plugins = [];
  switch (mode) {
    case 'production':
      plugins = defaultPlugins.concat(productionPlugins);
      break;
    case 'development':
      plugins = defaultPlugins.concat(developmentPlugins);
      break;
    // throw error if called without proper mode
    default:
      throw new Error(`You must run webpack either in development mode or production mode like this:
         webpack --mode 'development'
         or
         webpack --mode 'production'
        `);
  }

  return plugins;
};

module.exports = (env, argv) => {
  const isProduction = Boolean(argv.mode === 'production');
  return {
    entry: path.resolve(__dirname, 'src/index.jsx'),
    // resolve following paths for imports
    resolve: {
      modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    },
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          // extensions that the 'import' should resolve
          resolve: {
            extensions: ['.jsx', '.js'],
          },
          use: [
            {
              // transpiles ES6 to Javascript that all browsers defined in .browserlistrc will understand
              loader: 'babel-loader',
              options: {
                presets: ['react'],
              },
            },
            {
              // checks code style
              loader: 'eslint-loader',
              options: {
                failOnError: isProduction,
                failOnWarning: false,
                emitWarning: !isProduction,
              },
            },
          ],
        },
        {
          test: /\.scss$/,
          use: [
            {
              loader: !isProduction ? 'style-loader' : MiniCssExtractPlugin.loader,
              options: { sourceMap: true },
            },
            { // translates CSS into CommonJS
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                sourceMap: true,
                minimize: isProduction,
              },
            },
            // use autoprefixer to parse CSS and add vendor prefixes for supported browsers
            // it automatically uses the browsers defined in .browserlistrc
            {
              loader: 'postcss-loader',
              options: {
                sourceMap: true,
                plugins: () => [
                  autoprefixer(),
                ],
              },
            },
            { // compiles Sass to CSS
              loader: 'sass-loader',
              options: { sourceMap: true },
            },
          ],
        },
        {
          test: /\.(png|jpe?g|gif|svg)$/,
          use: [
            {
              // transforms files into base64 URIs (for less http requests)
              loader: 'url-loader',
              options: {
                limit: 30000, // only for files smaller than 30KB, otherwise fallback to file-loader
              },
            },
          ],
        },
      ],
    },
    // prevent "cannot GET /URL" error on refresh while working in dev mode
    devServer: {
      historyApiFallback: true,
    },
    // enable source maps http://cheng.logdown.com/posts/2016/03/25/679045
    // more options: https://webpack.js.org/configuration/devtool/
    devtool: isProduction ? false : 'cheap-module-eval-source-map',
    plugins: getPlugins(argv.mode),
  };
};
